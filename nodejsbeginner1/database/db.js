const oracledb = require('oracledb')

const config = {
    user: "system",
    password: "tiger",
    connectString: "localhost:1521/xe"
}
async function databaseconnnection(){
    await oracledb.getConnection(config)
        .then(pool => {
            console.log('Connected to Oracle')
            return pool
        })
        .catch(err => console.log('Database Connection Failed! Bad Config: ', err))

  
}

databaseconnnection();

module.exports = {
    config,oracledb, databaseconnnection
}