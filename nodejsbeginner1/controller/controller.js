const { config,oracledb} = require('../database/db')
const fs = require('fs-extra');
var rawdata = fs.readFileSync('./query/queries.json');
var queries = JSON.parse(rawdata);

class EmpController {

    async getAllData(req, res) {
        let connection;
        try {
            connection = await oracledb.getConnection(config);
            let result = await connection.execute(queries.getAllData);
            res.json(result.rows)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async addNewData(req, res) {
        try {
            if (req.body.empid != null && req.body.name != null && req.body.empcode != null && req.body.salary != null) {

                let connection = await oracledb.getConnection(config);

                let result = await connection.execute(queries.addNewUser,
                    { empid: { type: oracledb.NUMBER, val: req.body.empid }, name: req.body.name, empcode: req.body.empcode, salary: { type: oracledb.NUMBER, val: req.body.salary }}  // type and direction are optional for IN binds
                    
                    , { autoCommit: true });
                /*let result = await connection
                    .input('empid', oracledb.NUMBER, req.body.empid)
                    .input('name', oracledb.VarChar, req.body.name)
                    .input('empcode', oracledb.VarChar, req.body.empcode)
                    .input('salary', oracledb.NUMBER, req.body.salary)
                    .query(queries.addNewUser)*/
                
                res.json(result)
            } else {
                res.send('Please fill all the details!')
            }
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }

    async updateData(req, res) {
        try {
            if (req.body.empid != null && req.body.name != null && req.body.empcode != null && req.body.salary != null) {

                let connection = await oracledb.getConnection(config);

                let result = await connection.execute(queries.updateUserDetails,
                    { empid: { type: oracledb.NUMBER, val: req.body.empid }, name: req.body.name, empcode: req.body.empcode, salary: { type: oracledb.NUMBER, val: req.body.salary } }  // type and direction are optional for IN binds

                    , { autoCommit: true });
              
                res.json(result)
            } else {
                res.send('Please fill all the details!')
            }
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }
    async deleteData(req, res) {
        try {
            let connection = await oracledb.getConnection(config);
            if (req.body.empid != null) {
                let result = await connection.execute(queries.deleteUser,
                    { empid: { type: oracledb.NUMBER, val: req.body.empid }}  // type and direction are optional for IN binds
                    , { autoCommit: true });
                res.json(result)
            } else {
                res.send('empid is not exists')
            }
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }
    }



    //iteration
    async getAllDataIterate(req, res) {
        let connection;
        try {
            connection = await oracledb.getConnection(config);
            let result = await connection.execute(queries.getAllData);
            const finalresult = result.rows;
            console.log(finalresult);
            console.log('-----------------------------------');

            for (let row in finalresult) {
               // console.log(finalresult[row]); to get object
                console.log(finalresult[row].NAME);//to get object value
            }

            console.log('-----------------------------------');


            finalresult.forEach(row => {
                console.log(row.EMPID + '-' + row.EMPCODE + '-' + row.NAME + '-' + row.SALARY);
                if (row.EMPID == 4) {
                    row.NAME = 'ANIL';
                }
            }); 
            
            res.json(result.rows)
        } catch (error) {
            res.status(500)
            res.send(error.message)
        }

        

       /* const object = { a: 1, b: 2, c: 3 };

        for (const property in object) {
            console.log(`${property}: ${object[property]}`);
        }*/


       /* const object = { a: 1, b: 2, c: 3 };

        for (const property in object) {
            console.log(`${property}: ${object[property]}`);
        }*/

    }
    
}
const controller = new EmpController()
module.exports = controller;