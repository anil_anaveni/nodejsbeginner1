const express = require('express')
const oracledb = require('oracledb');
const morgan = require('morgan');
const fs = require('fs-extra');
const path = require('path');
oracledb.outFormat = oracledb.OBJECT;
const bodyParser = require('body-parser');
const router = require('./routes/route');
const app = express();
const port = 3000;
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, '/logs/access.log'), { flags: 'a' });

// setup the logger
app.use(morgan('combined', { stream: accessLogStream }));

app.use(router);
app.listen(process.env.PORT || port, (err) => {
    if (err)
        console.log('Unable to start the server!')
    else
        console.log('Server started running on : ' + port)
});




